# Copyright 1999-2003 Gentoo Technologies, Inc.
# Distributed under the terms of the GNU General Public License v2
# $Header$

inherit rpm

RESTRICT="nomirror primaryuri"

# NOTE: The comments in this file are for instruction and documentation.
# They're not meant to appear with your final, production ebuild.  Please
# remember to remove them before submitting or committing your ebuild.  That
# doesn't mean you can't add your own comments though.

# The 'Header' on the third line should just be left alone.  When your ebuild
# will be committed to cvs, the details on that line will be automatically
# generated to contain the correct data.

# Short one-line description of this package.
DESCRIPTION="okas - Just OKAS"

# Homepage, not used by Portage directly but handy for developer reference
HOMEPAGE="http://glen.alkohol.ee/okas/"

# Point to any required sources; these will be automatically downloaded by
# Portage.
SRC_URI="http://glen.alkohol.ee/okas/dist/${P}.rpm"

# License of the package. This must match the name of file(s) in
# /usr/portage/licenses/. For complex license combination see the developer
# docs on gentoo.org for details.
LICENSE="Ask glen@alkohol.ee"

# The SLOT variable is used to tell Portage if it's OK to keep multiple
# versions of the same package installed at the same time. For example,
# if we have a libfoo-1.2.2 and libfoo-1.3.2 (which is not compatible
# with 1.2.2), it would be optimal to instruct Portage to not remove
# libfoo-1.2.2 if we decide to upgrade to libfoo-1.3.2. To do this,
# we specify SLOT="1.2" in libfoo-1.2.2 and SLOT="1.3" in libfoo-1.3.2.
# emerge clean understands SLOTs, and will keep the most recent version
# of each SLOT and remove everything else.
# Note that normal applications should use SLOT="0" if possible, since
# there should only be exactly one version installed at a time.
# DO NOT USE SLOT=""! This tells Portage to disable SLOTs for this package.
SLOT="0"

# Using KEYWORDS, we can record masking information *inside* an ebuild
# instead of relying on an external package.mask file. Right now, you
# should set the KEYWORDS variable for every ebuild so that it contains
# the names of all the architectures with which the ebuild works. We have
# 4 official architecture names right now: "~x86", "~ppc", "~sparc"
# and "~alpha".  The ~ in front of the architecture indicates that the
# package is new and should be considered unstable until testing proves its
# stability.  Once packages go stable the ~ prefix is removed.
# So, if you've confirmed that your ebuild works on x86 and ppc,
# you'd specify: KEYWORDS="~x86 ~ppc"
# For packages that are platform-independent (like Java, PHP or Perl
# applications) specify all keywords.
# For binary packages, use -* and then list the archs the bin package
# exists for.  If the package was for an x86 binary package, then
# KEYWORDS would be set like this: KEYWORDS="-* x86"
# DO NOT USE KEYWORDS="*". This is deprecated and only for backward
# compatibility reasons.
KEYWORDS="x86"

# Comprehensive list of any and all USE flags leveraged in the ebuild,
# with the exception of any ARCH specific flags, i.e. "ppc", "sparc",
# "x86" and "alpha". This is a required variable. If the
# ebuild doesn't use any USE flags, set to "".
#IUSE="X gnome"

# Build-time dependencies, such as
#    ssl? ( >=dev-libs/openssl-0.9.6b )
#    >=dev-lang/perl-5.6.1-r1
# It is advisable to use the >= syntax show above, to reflect what you
# had installed on your system when you tested the package.  Then
# other users hopefully won't be caught without the right version of
# a dependency.
DEPEND="
"

# Run-time dependencies, same as DEPEND if RDEPEND isn't defined:
#RDEPEND=""

# Source directory; the dir where the sources can be found (automatically
# unpacked) inside ${WORKDIR}.  S will get a default setting of ${WORKDIR}/${P}
# if you omit this line.
S=${WORKDIR}

src_unpack() {
	rpm_unpack ${DISTDIR}/${P}.rpm
}

src_install() {
	dodir /usr/bin /usr/sbin /usr/lib/okas /usr/share/okas/vim /etc/skel /usr/share/man/man5

	exeinto /usr/bin
	doexe ${S}/usr/bin/*

	exeinto /usr/sbin
	doexe ${S}/usr/sbin/*

	insinto /usr/share/okas
	doins ${S}/usr/share/okas/*

	insinto /usr/share/okas/vim
	doins ${S}/usr/share/okas/vim/*

	insinto /usr/lib/okas
	doins ${S}/usr/lib/okas/functions

	exeinto /usr/lib/okas
	doexe ${S}/usr/lib/okas/*.sh
	doexe ${S}/usr/lib/okas/fw.*

	insinto /etc/skel
	doins ${S}/etc/skel/.??*

	insinto /usr/share/man/man5
	doman ${S}/usr/share/man/man5/*
}


pkg_postinst() {
	${ROOT}usr/lib/okas/postin.sh
}

pkg_prerm() {
	${ROOT}usr/lib/okas/preun.sh
}
