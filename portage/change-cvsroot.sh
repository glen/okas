#!/bin/sh
#CVSROOT=':ext:cvs.delfi.ee:/usr/local/cvs'
CVSROOT=':ext:cvs.delfi.ee:/home/glen/CVSROOT'
find -name Root | xargs -ri sh -c "echo '$CVSROOT' > {}"

#REPO='scripts/okas/gentoo'
REPO='gentoo/portage/sys-apps/okas'
find -name Repository | while read file; do
	repo=`echo "$file" | cut -c3- | sed -e 's,CVS/Repository,,'`
   	repo=`echo "$REPO/$repo" | sed -e 's,/$,,'`
	echo "$repo" > $file
done
