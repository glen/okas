#!/bin/sh
# $Id$

. /usr/lib/okas/functions

# allow setup stright from cvs sources
if [ $(basename "$0") = install.sh ]; then
	dir=$(dirname "$0")
	lib=$(cd "$dir"; pwd)
else
	lib=/usr/share/okas/ssh-agent
fi

files='bashrc bash_profile bash_logout'

install() {
	for file in $files; do
		# make sure it exist, all files required
		touch "$HOME/.$file"
		PatchFile "$HOME/.$file" \
			"\..*okas/ssh-agent/$file" \
			"s|^#\?..*okas/ssh-agent/$file|. $lib/$file|g" \
			". $lib/$file"
	done
}

uninstall() {
	for file in $files; do
		UnPatchFile "$HOME/.$file" \
			"s|^..*okas/ssh-agent/$file|#&|g"
	done
}

case "$1" in
-i|--install)
	install
	;;

-u|--uninstall)
	uninstall
	;;

--start|start)
	. /usr/share/okas/ssh-agent/bash_profile
	;;

--stop|stop)
	. /usr/share/okas/ssh-agent/bash_logout
	;;

esac
