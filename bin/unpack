#!/bin/sh
# Author: glen@delfi.ee
# Generic unpacker.
# File extension patterns taken from lesspipe.sh.

self="${0##*/}"

# parse command line args
t=$(getopt -o hv --long help,verbose -n "$self" -- "$@")
[ $? != 0 ] && exit $?
eval set -- "$t"

usage="Usage: $self [options]
Generic unpacker.

  -h|--help                    display this help and exit.
  -v|--verbose                 verbose unpacking output (if applicable)
"

unset verbose

while :; do
	case "$1" in
	-h|--help)
		echo "$usage"
		exit 0
	;;
	-v|--verbose)
		verbose=1
	;;
	--)
		shift
		break
	;;
	*)
		echo 2>&1 "$self: Internal error: [$1] not recognized!"
		exit 1
	;;
	esac
	shift
done

if [ $# = 0 ]; then
	echo >&2 "$usage"
	exit 1
fi

unpack_deb() {
	local deb=$1

	if [ -x /usr/bin/dpkg-deb ]; then
		dpkg-deb -x "$deb" .
		return $?
	fi

	ar x "$1"
	unpack control.tar.gz
	rm control.tar.gz
	unpack data.tar.gz
	rm data.tar.gz
}

unpack_zip() {
	local file=$1

	if [ -x /usr/bin/7z ]; then
		7z x "$file"
	else
		unzip -a "$file"
	fi
}

unpack_cpio() {
	cpio --no-absolute-filenames -dimu${verbose:+v} --quiet
}

unpack_initrd() {
	local file=$1 magic

	magic=$(file -z "$1" 2>/dev/null)
	case "$magic" in
	*cpio?archive*)
		gzip -dc "$1" | unpack_cpio
		;;
	esac
}

unpack() {
	echo -n "* $1... "

	case "$1" in
		*.tgz|*.tar.[Zz]|*.tar.gz) tar zx${verbose:+v}f "$1" ;;
		*.tbz2|*.tbz|*.tar.bz2) tar jx${verbose:+v}f "$1" ;;
		*.tar.xz) xz -dc "$1" | tar x${verbose:+v} ;;
		*.tar.lzma) lzma -dc "$1" | tar x${verbose:+v} ;;
		*.tar) tar x${verbose:+v}f "$1" ;;
		*.cpio.gz|*.cgz) gzip -dc -- "$1" | unpack_cpio ;;
		*.uue) uudecode < "$1" ;;
		*.a) ar x${verbose:+v}f "$1" ;;
		*.bz2) bzip2 -d${verbose:+v} "$1" ;;
		*.deb) unpack_deb "$1" ;;
		*.lrz) lrzip -d "$1" ;;
		*.phar) phar extract -f "$1" >/dev/null ;;
		*.zip|*.ZIP|*.pk3) unpack_zip "$1" ;;
		*.7z) 7z x "$1" ;;
		*.jar) jar xvf "$1" ;;
		*initrd-*.gz) unpack_initrd "$1" ;;
		*.cpio|*.cpi) unpack_cpio < "$1" ;;
		*.rpm) rpm2cpio "$1" | unpack_cpio ;;
		*.[Rr][Aa][Rr]) unrar -ierr x "$1" ;;
		*.ace) unace x "$1" ;;
		*)
			 echo >&2 "Don't know how to unpack $1"
			 return 1
		;;
	esac

	echo "DONE"
}

# unpack all sources in commandline
for a in "$@"; do
	unpack "$a"
done
