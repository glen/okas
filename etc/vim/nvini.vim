" Vim syntax file
" Language:	ini file for parser and adminui
" Maintainer:	glen@delfi.ee
" Last Change:	16 Dec, 1999
" Last Change:	02/02/2000

" Remove any old syntax stuff hanging around
syn clear
" syntax is case sensitive
syn case match

syn region	nviniVariable		start="%" end="%"
syn match	nviniSpecial		"\\$"
if exists("nvini_want_commas")
	syn match	nviniSpecial		";"
	syn match	nviniOptions		","
endif
syn region	nviniAdminVars		start="\[" end="\]" contained

syn region	nviniString			start="'" skip="\\\\\|\\'" end="'" contains=nviniVariable

syn match	nviniGeners			"^.*\(Preview\|Gener\(Category\)*\(\d\)*\):"me=e-1 contains=nviniVariable
syn match	nviniGenerateAll	"^\(generateAllGener\(\d\)*\):"me=e-1 contains=nviniVariable
syn match	nviniGener			"^.*\(Preview\|Gener\(Category\)*\(\d\)*\):.*" contains=nviniGeners,nviniGenerateAll,nviniAdminVars,nviniVariable

syn match	nviniDefs			"^.*\(Def\|Menu\|XRef\):"me=e-1 contains=nviniVariable
syn match	nviniOptions		"^.*\(Options\|Defaults\|\(Menu\|Item\)\(Start\|End\|ScriptInclude\|AdminClass\)\):"me=e-1 contains=nviniVariable
syn match	nviniMenuDef		"^.*\(Menu\):.*"me=e-1 contains=nviniDefs,nviniAdminVars,nviniVariable

syn match	nviniKeyword		"^\(TypeClasses\|DefaultValueTypeClasses\|MainDef\|TableList\|TableListeners\|XRefTableList\|generate\(First\|Last\)\):"me=e-1

syn keyword	nviniTodo		contained TODO
syn match	nviniComment	"#.*$" contains=nviniTodo

if !exists("did_nvini_syntax_inits")
  let did_nvini_syntax_inits = 1
  " The default methods for highlighting.  Can be overridden later
  hi link nviniComment		Comment
  hi link nviniTodo			Todo
  hi link nviniVariable		Statement
  hi link nviniSpecial		Special
  hi link nviniKeyword		Type
  hi link nviniDefs			Identifier
  hi link nviniGeners		Type
  hi link nviniGenerateAll	Constant
  hi link nviniOptions		Conditional
  hi link nviniAdminVars	Conditional
  hi link nviniString		String
endif

let b:current_syntax = "nvini"
" vim: ts=4
