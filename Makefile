# $Id$

# FHS paths
prefix		:= /usr
ifeq ($(shell uname), Darwin)
prefix := $(prefix)/local
endif
sysconfdir	:= /etc
libdir		:= $(prefix)/lib
sbindir		:= $(prefix)/sbin
bindir		:= $(prefix)/bin
mandir		:= $(prefix)/share/man
datadir		:= $(prefix)/share
vimdatadir	:= $(prefix)/share/vim/vimfiles

SHELLTOOLS := $(shell grep -oE '^=item B<[^>]+' doc/okas.pm | grep -oE '[^<]+$$')

all: man

man:
	$(MAKE) -C doc

install-man: man
	install -d $(DESTDIR)$(mandir)/man{1,5}
	cp -p doc/okas.5 $(DESTDIR)$(mandir)/man5
	echo '.so man5/okas.5' > $(DESTDIR)$(mandir)/man1/.link.1; \
	for a in $(SHELLTOOLS); do \
		ln $(DESTDIR)$(mandir)/man1/{.link.1,$$a.1}; \
	done; \
	rm -f $(DESTDIR)$(mandir)/man1/rgrep.1; \
	rm -f $(DESTDIR)$(mandir)/man1/.link.1;
	cp -p doc/fannotate.1 $(DESTDIR)$(mandir)/man1

install: install-man
	install -d $(DESTDIR)$(bindir)
	install -d $(DESTDIR)$(sbindir)
	install -d $(DESTDIR)$(sysconfdir)
	install -d $(DESTDIR)$(datadir)/okas/ssh-agent
	install -d $(DESTDIR)$(datadir)/okas/vim
	install -d $(DESTDIR)$(libdir)/okas

	install -p bin/{cvsstat,diffcol} $(DESTDIR)$(bindir)
	install -p bin/less.linux $(DESTDIR)$(bindir)
	install -p bin/less-color $(DESTDIR)$(bindir)
	install -p bin/cronlogger $(DESTDIR)$(bindir)
	install -p bin/fannotate $(DESTDIR)$(bindir)

	install -p bin/mysql_restore $(DESTDIR)$(bindir)
	install -p bin/pcharmap $(DESTDIR)$(bindir)
	install -p bin/utime $(DESTDIR)$(bindir)
	install -p bin/killtails $(DESTDIR)$(bindir)
	install -p bin/wr $(DESTDIR)$(bindir)
	install -p bin/sp $(DESTDIR)$(bindir)
	install -p bin/dus $(DESTDIR)$(bindir)
	install -p bin/boottime $(DESTDIR)$(bindir)
	install -p bin/svn-clone $(DESTDIR)$(bindir)
	install -p bin/git-pullify $(DESTDIR)$(bindir)

	install -p bin/{nt,ws,rtar,sush,xapp,xtitle,scr-ssh,ssh-user} $(DESTDIR)$(bindir)
	install -p bin/{pkgbytime,make-ignores,quietrun} $(DESTDIR)$(bindir)
	install -p bin/mkisoimage $(DESTDIR)$(bindir)
	install -p bin/unpack $(DESTDIR)$(bindir)
	install -p bin/xdif $(DESTDIR)$(bindir)

	install -p sbin/{apache_logs,cocalores,findsuid} $(DESTDIR)$(sbindir)
	install -p sbin/backup-{dirs,mysql{,.pl},rsync} $(DESTDIR)$(sbindir)
	install -p sbin/log-knock-check $(DESTDIR)$(sbindir)
	install -p sbin/srv-cvs-init $(DESTDIR)$(sbindir)
	install -p sbin/vifup $(DESTDIR)$(sbindir)
	ln -s vifup $(DESTDIR)$(sbindir)/vifdown
	install -p sbin/ps_mem $(DESTDIR)$(sbindir)

	cp -p lib/{functions,*.sh} $(DESTDIR)$(libdir)/okas

	install -p ssh-agent/install.sh $(DESTDIR)$(bindir)/okas-ssh-agent
	install -p ssh-agent/bash* $(DESTDIR)$(datadir)/okas/ssh-agent

	cp -p etc/vim/okas.vim $(DESTDIR)$(datadir)/okas/vim
	cp -p etc/vim/vimrc $(DESTDIR)$(datadir)/okas/vim

	cp -p etc/{bashrc,{cdhist,fg}.bash} $(DESTDIR)$(datadir)/okas
	cp -p etc/ssh-auth-sock $(DESTDIR)$(datadir)/okas
	cp -p etc/glen.{kmap,map} $(DESTDIR)$(datadir)/okas
	cp -p etc/{sysctl.conf,screenrc,XTerm.ad,DIR_COLORS} $(DESTDIR)$(datadir)/okas
	cp -p etc/df.awk $(DESTDIR)$(datadir)/okas
	cp -p etc/okas.asc $(DESTDIR)$(datadir)/okas

	cp -p etc/bashrc.local $(DESTDIR)$(sysconfdir)

	cp -p etc/yum.repo $(DESTDIR)$(datadir)/okas
	cp -p etc/poldek.repo $(DESTDIR)$(datadir)/okas
	cp -p etc/apt.repo $(DESTDIR)$(datadir)/okas

	cp -p etc/subversion $(DESTDIR)$(datadir)/okas
