#!/bin/sh

die() {
	rc=$?
	echo >&2 "$*"
	exit $rc
}

PatchLine() {
	local file="$1"; shift
	[ -n "$file" ] || die "No file to patch ($file)"
	[ -f "$file" ] || return 0
	local sed="$1"; shift

	# NOTE: using cp to preservee permissions and ownership.
	# FIXME: this could file truncation on error.

	cp -f "$file" "$file.rpmorig" || die "cp failed"
	sed "$sed" < "$file.rpmorig" > "$file" || {
		mv -f "$file.rpmorig" "$file"
		die "sed failed"
	}
	rm -f "$file.rpmorig"
}

# Find Include Statement or add it if necessary
# void PatchFile(char *file, char *grep, char *sed, char *echo)
PatchFile() {
	local file="$1"; shift
	[ -n "$file" ] || die "No file to patch ($file)"
	[ -f "$file" ] || return 0

	local grep="$1"; shift
	local sed="$1"; shift

	if grep -Eq "$grep" "$file"; then
		# found. Insert our include statement here
		PatchLine "$file" "$sed"
	else
		if [ "$1" ]; then
			local echo="$1"; shift
			echo "" >> "$file"
			echo "$echo" >> "$file"
		fi
	fi
}

# Find related configuration settings and comment them out
UnPatchFile() {
	local file="$1"; shift
	local sed="$1"; shift

	PatchLine "$file" "$sed"
}

dosed() {
	local file="$1"; shift

	cp -f "$file" "$file.rpmorig" || die "cp failed"
	sed -e "$@" < "$file.rpmorig" > "$file" || {
		mv -f "$file.rpmorig" "$file"
		die "sed failed"
	}
	rm -f "$file.rpmorig"
}
