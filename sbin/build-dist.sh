#!/bin/sh
set -e
set -x

distdir=$(dirname "$0")
cd $distdir

pkg_name=$1
pkg_version=$2
pkg_release=$3

rpm=${pkg_name}-${pkg_version}-${pkg_release}.noarch.rpm
deb=${pkg_name}_${pkg_version}-${pkg_release}_all.deb

ls -l $rpm
ls -l $deb

# poldek repo
rm -f $pkg_name.rpm
poldek --cachedir=$HOME/tmp --mkidx -s . --mt=pndir

# yum repo
createrepo .

# for manual quick download
ln -sf $rpm $pkg_name.rpm

# apt repo (debian/ubuntu)
rm -f $pkg_name.deb
dpkg-scanpackages . /dev/null > .Packages.tmp
mv .Packages.tmp Packages

# create Packgages.bz2
bzip2 -9 < Packages > .Packages.tmp.bz2
mv .Packages.tmp.bz2 Packages.bz2

gzip -9 < Packages > .Packages.tmp.gz
mv .Packages.tmp.gz Packages.gz

# for manual quick download
ln -sf $deb $pkg_name.deb
